# cl-penumbra

A small 3D OpenGL framework for Common Lisp (SBCL)

(inspired by the book "Developing Graphics Frameworks with Python anda OpenGL"
 by Lee Stemkoski and Michael Pascale published by CRC Press in 2021)

**Pre-alpha status** : the projet is not yet published officially

## Getting started

(todo)

## License

Copyright (C) Frederic Peschanski under under the MIT License (cf. LICENSE file)

