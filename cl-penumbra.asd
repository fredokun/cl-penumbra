
(asdf:defsystem #:cl-penumbra
    :description "A Small 3D OpenGL Framework for Common Lisp"
    :version "0.1"
    :author "Frederic Peschanski <first.last@lip6.fr>"
    :licence "MIT"
    :depends-on (:cffi :trivial-features)
    :pathname "src"
    :serial t
    :components ((:file "package")
                 (:file "cl-penumbra")))


