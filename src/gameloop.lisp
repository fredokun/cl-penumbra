
(defstruct (gamestate (:conc-name game-))
  graphic-context
  window
  (status :uninitialized))


(defun game-init (&key 
                    (title "cl-penumbra")
                    (width 800) (height 600)
                    (vsync nil) (visibible t))
  (glfw:initialize)
  (let ((game (make-gamestate :graphic-context nil :window nil :status :uninitialized)))
    game))


